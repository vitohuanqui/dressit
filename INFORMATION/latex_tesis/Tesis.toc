\select@language {spanish}
\contentsline {chapter}{\numberline {1}Introducción}{2}
\contentsline {section}{\numberline {1.1}Motivación y Contexto}{3}
\contentsline {section}{\numberline {1.2}Planteamiento del Problema}{4}
\contentsline {section}{\numberline {1.3}Objetivos}{4}
\contentsline {subsection}{\numberline {1.3.1}Objetivos Específicos}{4}
\contentsline {section}{\numberline {1.4}Organización de la tesis}{4}
\contentsline {chapter}{\numberline {2}Marco Teórico}{6}
\contentsline {section}{\numberline {2.1}Sustracción de Fondo}{6}
\contentsline {section}{\numberline {2.2}Operaciones Morfológicas}{6}
\contentsline {subsection}{\numberline {2.2.1}Dilatación Binaria}{7}
\contentsline {subsubsection}{Propiedades}{7}
\contentsline {subsection}{\numberline {2.2.2}Erosión Binaria}{8}
\contentsline {subsubsection}{Propiedades}{8}
\contentsline {subsection}{\numberline {2.2.3}Apertura}{8}
\contentsline {subsubsection}{Teorema de Caracterización}{9}
\contentsline {subsection}{\numberline {2.2.4}Clausura}{9}
\contentsline {subsubsection}{Propiedades}{9}
\contentsline {section}{\numberline {2.3}Shape Context}{9}
\contentsline {subsection}{\numberline {2.3.1}Características}{10}
\contentsline {subsection}{\numberline {2.3.2}Algoritmo Húngaro}{10}
\contentsline {subsection}{\numberline {2.3.3}Segmentación de Imágenes (Cuerpo))}{11}
\contentsline {section}{\numberline {2.4}Autómata Celular}{11}
\contentsline {subsubsection}{Elementos}{12}
\contentsline {subsection}{\numberline {2.4.1}GrowCut}{12}
\contentsline {section}{\numberline {2.5}\textit {Convolutional Neural Networks (CNNs / ConvNets)}}{13}
\contentsline {subsubsection}{Descripción de la Arquitectura}{13}
\contentsline {subsubsection}{Capas}{13}
\contentsline {subsection}{\numberline {2.5.1}\textit {TensorFlow}}{14}
\contentsline {subsection}{\numberline {2.5.2}\textit {Red convolucional VGGNet}}{14}
\contentsline {section}{\numberline {2.6}Estado del Arte}{14}
\contentsline {subsection}{\numberline {2.6.1}Sustracción de \textit {Background} - Detección de \textit {ForeGround}}{14}
\contentsline {subsection}{\numberline {2.6.2}Detección y Segmentación de la Silueta}{17}
\contentsline {subsection}{\numberline {2.6.3}Reconocimiento de Características Específicas}{19}
\contentsline {chapter}{\numberline {3}Propuesta}{21}
\contentsline {section}{\numberline {3.1}Optimización de \textit {GrowCut}}{25}
\contentsline {chapter}{\numberline {4}Pruebas y Resultados}{27}
\contentsline {section}{\numberline {4.1}Conjunto de Datos}{27}
\contentsline {subsection}{\numberline {4.1.1}Datos Enfocados en YOLO}{27}
\contentsline {subsection}{\numberline {4.1.2}Datos Enfocados en \textit {Shape Context}}{28}
\contentsline {subsection}{\numberline {4.1.3}Datos Enfocados en \textit {CNN VGGNet}}{28}
\contentsline {subsection}{\numberline {4.1.4}Datos Enfocados en el Proceso Completo}{28}
\contentsline {section}{\numberline {4.2}Diseño del Experimento}{29}
\contentsline {subsection}{\numberline {4.2.1}Participantes}{29}
\contentsline {subsection}{\numberline {4.2.2}Diseño de la Propuesta}{29}
\contentsline {section}{\numberline {4.3}Evaluación del Programa Completo}{31}
\contentsline {chapter}{\numberline {5}Conclusiones y Trabajos Futuros}{35}
\contentsline {section}{\numberline {5.1}Trabajos Futuros}{36}
\contentsline {chapter}{Bibliografía}{41}
