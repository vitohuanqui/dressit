ln -s /usr/local/lib/python2.7/dist-packages/cv2.so ../lib/python2.7/site-packages/cv2.so
or
ln -s /usr/local/lib/python2.7/site-packages/cv2.so ../lib/python2.7/site-packages/cv2.so

sudo apt-get install python-matplotlib python-numpy python-pil python-scipy

sudo apt-get install build-essential cython

sudo apt-get install python-skimage

python classify.py --model fashion.model --labelbin mlb.pickle --image examples/example_01.jpg