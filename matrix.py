# This file was automatically generated by SWIG (http://www.swig.org).
# Version 3.0.8
#
# Do not make changes to this file unless you know what you are doing--modify
# the SWIG interface file instead.





from sys import version_info
if version_info >= (2, 6, 0):
    def swig_import_helper():
        from os.path import dirname
        import imp
        fp = None
        try:
            fp, pathname, description = imp.find_module('_matrix', [dirname(__file__)])
        except ImportError:
            import _matrix
            return _matrix
        if fp is not None:
            try:
                _mod = imp.load_module('_matrix', fp, pathname, description)
            finally:
                fp.close()
            return _mod
    _matrix = swig_import_helper()
    del swig_import_helper
else:
    import _matrix
del version_info
try:
    _swig_property = property
except NameError:
    pass  # Python < 2.2 doesn't have 'property'.


def _swig_setattr_nondynamic(self, class_type, name, value, static=1):
    if (name == "thisown"):
        return self.this.own(value)
    if (name == "this"):
        if type(value).__name__ == 'SwigPyObject':
            self.__dict__[name] = value
            return
    method = class_type.__swig_setmethods__.get(name, None)
    if method:
        return method(self, value)
    if (not static):
        if _newclass:
            object.__setattr__(self, name, value)
        else:
            self.__dict__[name] = value
    else:
        raise AttributeError("You cannot add attributes to %s" % self)


def _swig_setattr(self, class_type, name, value):
    return _swig_setattr_nondynamic(self, class_type, name, value, 0)


def _swig_getattr_nondynamic(self, class_type, name, static=1):
    if (name == "thisown"):
        return self.this.own()
    method = class_type.__swig_getmethods__.get(name, None)
    if method:
        return method(self)
    if (not static):
        return object.__getattr__(self, name)
    else:
        raise AttributeError(name)

def _swig_getattr(self, class_type, name):
    return _swig_getattr_nondynamic(self, class_type, name, 0)


def _swig_repr(self):
    try:
        strthis = "proxy of " + self.this.__repr__()
    except Exception:
        strthis = ""
    return "<%s.%s; %s >" % (self.__class__.__module__, self.__class__.__name__, strthis,)

try:
    _object = object
    _newclass = 1
except AttributeError:
    class _object:
        pass
    _newclass = 0


class intMatrix(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, intMatrix, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, intMatrix, name)
    __repr__ = _swig_repr

    def __init__(self, *args):
        this = _matrix.new_intMatrix(*args)
        try:
            self.this.append(this)
        except Exception:
            self.this = this

    def change(self, x, y, value):
        return _matrix.intMatrix_change(self, x, y, value)

    def increase(self, x, y):
        return _matrix.intMatrix_increase(self, x, y)

    def __call__(self, x, y):
        return _matrix.intMatrix___call__(self, x, y)

    def _print(self):
        return _matrix.intMatrix__print(self)

    def getData(self):
        return _matrix.intMatrix_getData(self)
    __swig_destroy__ = _matrix.delete_intMatrix
    __del__ = lambda self: None
intMatrix_swigregister = _matrix.intMatrix_swigregister
intMatrix_swigregister(intMatrix)

class doubleMatrix(_object):
    __swig_setmethods__ = {}
    __setattr__ = lambda self, name, value: _swig_setattr(self, doubleMatrix, name, value)
    __swig_getmethods__ = {}
    __getattr__ = lambda self, name: _swig_getattr(self, doubleMatrix, name)
    __repr__ = _swig_repr

    def __init__(self, *args):
        this = _matrix.new_doubleMatrix(*args)
        try:
            self.this.append(this)
        except Exception:
            self.this = this

    def change(self, x, y, value):
        return _matrix.doubleMatrix_change(self, x, y, value)

    def increase(self, x, y):
        return _matrix.doubleMatrix_increase(self, x, y)

    def __call__(self, x, y):
        return _matrix.doubleMatrix___call__(self, x, y)

    def _print(self):
        return _matrix.doubleMatrix__print(self)

    def getData(self):
        return _matrix.doubleMatrix_getData(self)
    __swig_destroy__ = _matrix.delete_doubleMatrix
    __del__ = lambda self: None
doubleMatrix_swigregister = _matrix.doubleMatrix_swigregister
doubleMatrix_swigregister(doubleMatrix)

# This file is compatible with both classic and new-style classes.


