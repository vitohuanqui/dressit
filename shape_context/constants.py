CANNY = 1

T_CONSTANT_FIRST = [[2.46388436], [2.01368015], [1.57886067], [1.69949283],
                    [3.12198732],
                    [3.07225952], [3.12159532], [2.94419709], [3.01950352],
                    [3.13117636], [3.13300917], [3.04511888], [2.82910109],
                    [2.5405189], [2.3033895], [1.90617677], [2.05624733],
                    [1.97520525], [2.04846171], [2.29513511], [1.62380784],
                    [1.91008894], [1.7907106], [1.75306445], [2.25737944],
                    [2.19863292], [2.21960947], [1.64154419], [3.10012267],
                    [3.12435298], [3.02024065], [2.08994244], [2.20304088],
                    [2.72336832], [1.65426727], [3.00845633], [2.94816533],
                    [2.27551891], [1.9202476], [2.07993843], [2.91717664],
                    [1.99221553], [2.2880509], [2.3317005], [2.27343465],
                    [2.19504259], [2.14394394], [2.35815912], [2.3215851],
                    [2.05991613], [2.20406107], [1.78001788], [2.23322262],
                    [2.04779873], [1.59919778], [2.59270903], [3.10827166],
                    [2.60578142], [3.07067354], [3.13762442], [3.12949647],
                    [3.11194033], [3.13258389], [2.79389982], [2.89463771],
                    [3.04811587], [3.11420225], [1.73410169], [3.11595725],
                    [1.77078928], [3.10208753], [2.10009867], [3.10161397],
                    [2.30012028], [2.38673489], [1.70386359], [2.15783275],
                    [2.36215863], [1.85428805], [2.0215622], [2.10660756],
                    [2.93049932], [2.9039345], [2.96173915], [3.13752764],
                    [3.06765362], [3.06176267], [3.09475194], [3.10672307],
                    [3.10964006], [3.08206043], [2.49809154], [3.12324609],
                    [2.96257738], [3.11659786], [2.40465536], [2.24602294],
                    [2.36014704], [2.13985383], [1.70869112]]

SHAPE_BODY_FIRST = [(182, 133), (182, 114), (201, 172), (189, 127), (187, 119),
                    (204, 173), (208, 183), (186, 111), (208, 178), (200, 150),
                    (185, 102), (213, 176), (219, 179), (217, 170), (218, 167),
                    (213, 152), (218, 161), (208, 135), (206, 125), (206, 121),
                    (200, 104), (198, 92), (198, 83), (198, 67), (191, 54),
                    (119, 56), (115, 66), (114, 76), (114, 86), (114, 95),
                    (107, 119), (108, 118), (92, 164), (96, 157), (104, 137),
                    (134, 51), (96, 168), (97, 174), (99, 180), (101, 182),
                    (104, 180), (111, 157), (111, 161), (111, 164), (118, 140),
                    (111, 174), (111, 178), (124, 124), (129, 107), (130, 117),
                    (126, 145), (130, 124), (130, 128)]

SHAPE_LEGS_FIRST = [(152, 303), (154, 310), (159, 307), (162, 298), (159, 192),
                    (164, 283), (165, 281), (165, 275), (165, 269), (164, 232),
                    (165, 238), (174, 296), (174, 285), (176, 279), (181, 264),
                    (181, 236), (183, 245), (181, 219), (184, 204), (186, 189),
                    (127, 197), (130, 212), (129, 245), (130, 240), (132, 233),
                    (134, 272), (137, 281), (138, 288), (142, 311), (147, 243),
                    (147, 273), (147, 312), (148, 262)]


SHAPE_HEAD_FIRST = [(151, 0), (170, 30), (174, 22), (171, 9), (163, 1),
                    (143, 5), (140, 14)]



SHAPE_HEAD_SECOND = [(157, 0), (158, 0), (159, 0), (144, 5), (160, 6), (165, 12),
              (168, 18), (168, 20), (135, 36), (171, 40), (135, 48), (185, 54)]

SHAPE_LEGS_SECOND = [(142, 252), (172, 252), (156, 256), (171, 258), (156, 259),
              (156, 262), (144, 264), (144, 265), (145, 270), (153, 273),
              (126, 275), (171, 276), (145, 280), (150, 282), (144, 285),
              (120, 287), (119, 288), (141, 288), (170, 288), (144, 290),
              (162, 292), (168, 292), (118, 294), (131, 294), (140, 294),
              (140, 295), (147, 298), (176, 180), (140, 183), (176, 186),
              (175, 189), (117, 192), (175, 192), (156, 196), (136, 198),
              (175, 198), (176, 201), (135, 204), (117, 208), (118, 210),
              (160, 210), (177, 215), (178, 216), (120, 220), (120, 222),
              (120, 224), (121, 228), (161, 228), (123, 234), (160, 236),
              (176, 238), (139, 240), (175, 240), (175, 242), (125, 246),
              (126, 248), (126, 250), (119, 150), (119, 156), (120, 163),
              (150, 164), (186, 165), (185, 166), (120, 168), (151, 168),
              (175, 168), (180, 168), (182, 168), (120, 170), (144, 173),
              (176, 174), (176, 179), (141, 180)]

SHAPE_BODY_SECOND = [(186, 56), (188, 60), (189, 64), (132, 74), (129, 84), (191, 96),
              (192, 103), (192, 105), (123, 114), (121, 120), (120, 122),
              (120, 124), (195, 130), (195, 136), (117, 144)]

T_CONSTANT_SECOND = [[0.27094685], [1.21202566], [2.64604098], [-0.87605805],
                     [3.94127556], [3.76311928], [3.51423756], [3.50308663],
                     [0.05580791], [3.64060726], [-0.32175055], [3.91160742],
                     [3.63265955], [3.56755614], [3.39714175], [-0.28309579],
                     [-0.33709105], [3.11966634], [3.49213106], [3.45604407],
                     [-0.31874756], [-0.36322609], [-0.34697761], [-0.34209658],
                     [3.09489765], [3.12723957], [0.42930307], [0.14641024],
                     [-0.26625205], [0.32361275], [0.30825812], [2.49410353],
                     [2.38806775], [-0.29618794], [0.17442088], [3.5139911],
                     [1.47874948], [2.11877582], [-0.26003129], [2.61113015],
                     [3.12324609], [3.15080898], [2.64190752], [3.13237632],
                     [2.5959376], [3.10369611], [2.89989985], [-0.02241777],
                     [3.08854512], [0.46543972], [2.81153204], [3.37066079],
                     [3.49836931], [3.01500053], [0.59011677], [0.56263807],
                     [0.4318562], [3.29239968], [3.17043081], [0.31613264],
                     [0.18982632], [0.21866895], [0.3108518], [-0.02192631],
                     [0.3332443], [-0.02911798], [2.89441619], [3.17015631],
                     [2.70472436], [2.82703622], [0.30704573], [0.19048721],
                     [0.05424537], [3.18422084], [2.90405802], [0.02314401],
                     [2.10450452], [-0.24186664], [-0.33753117], [3.66266834],
                     [3.56822015], [3.08091955], [-0.43358148], [-0.06269798],
                     [2.98011503], [3.14666875], [-0.56273722], [3.09717744],
                     [-0.86217005], [-0.71922984], [1.62307423], [3.07533905],
                     [-0.93655421], [2.62860241], [1.98902066], [0.76972547],
                     [1.95400784], [-0.35470565], [-0.02617203], [1.62561938]]
