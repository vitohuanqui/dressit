"""" Implementation of the grow-cut algorithm (numpy version) """

from __future__ import division

import numpy as np

from skimage import img_as_float
from math import sqrt

import logging


def G(x, y):
    return 1 - np.sqrt((x - y) ** 2) / sqrt(3)


def Gnorm(x, y):
    rest = (x - y).astype(int)
    n_sqrt = np.sqrt(np.sum(rest ** 2, 2)) / (sqrt(3)*255)
    matrix = np.zeros(x.shape)
    for cor in np.ndindex(matrix.shape[:2]):
        matrix[cor] = n_sqrt[cor]
    return 1 - matrix


def growcut(image, state, max_iter=100, window_size=5):
    """Grow-cut segmentation.
    Parameters
    ----------
    image : (M, N) ndarray
        Input image.
    state : (2, M, N) ndarray
        Initial state, which stores (foreground/background, strength) for
        each pixel position or automaton.  The strength represents the
        certainty of the state (e.g., 1 is a hard seed value that remains
        constant throughout segmentation).
    max_iter : int, optional
        The maximum number of automata iterations to allow.  The segmentation
        may complete earlier if the state no longer varies.
    window_size : int
        Size of the neighborhood window.
    Returns
    -------
    mask : ndarray
        Segmented image.  A value of zero indicates background, one foreground.
    """
    image = img_as_float(image)
    height, width = image.shape[:2]
    ws = (window_size - 1) // 2

    state_next = state.copy()

    changing = 1

    for n in range(0, max_iter):

        if not changing:
            break

        changing = 0
        count = 0

        for coord in np.ndindex(height, width):
            i, j = coord
            C_p = image[i, j]
            S_p = state[:, i, j]

            window = [
                slice(max(0, i - ws), min(i + ws + 1, height)),
                slice(max(0, j - ws), min(j + ws + 1, width))
                ]
            C_q = image[window]
            S_q_label = state[0][window]
            S_q_strength = state[1][window]
            if not np.any(S_q_strength):
                count += 1
                continue
            # print C_q[0]
            # print S_q_label[0]
            # print S_q_strength[0]

            gc = G(C_q, C_p)

            # Compute the strength mask
            mask = (gc * S_q_strength) > S_p[1]
            # mask = (gc * S_q_strength) > S_p[1]
            # print '-------------------'
            # print S_q_strength
            # print gc
            # print mask
            # print np.any(mask)
            # print S_p[1]

            if np.any(mask):
                state_next[0, i, j] = (S_q_label[mask])[0]
                state_next[1, i, j] = (gc * S_q_strength)[mask][0]
                changing += 1
        state = state_next
    print(np.any(state[0]))
    return state[0]


def growcut2(img, points, window_size=5, iterations=10):
    """
    a = {(x,y): label, strength}
    C_p = current pixel
    C_q = current neighbors
    gc = distancia entre c_p y c_q entre 0 y 1
    loop_strength = 0 -> loop, 1->label
    :param img:
    :param points:
    :param window_size:
    :return:
    """

    loop_label_strength = np.zeros((img.shape[0], img.shape[1], 3, 3))
    ws = (window_size - 1) // 2
    height, width = img.shape[:2]
    for k in points:
        loop_label_strength[k[0], k[1], 0] = 1
        loop_label_strength[k[0], k[1], 1] = points[k][0]
        loop_label_strength[k[0], k[1], 2] = points[k][1]
    while True:
        for p in points.keys():
            x, y = p
            C_p = img[x, y]
            max_x = max(0, x - ws)
            max_y = max(0, y - ws)
            min_x = min(x + ws + 1, height)
            min_y = min(y + ws + 1, width)
            window = [
                slice(max_x, min_x),
                slice(max_y, min_y)
            ]
            C_q = img[window]
            gc = Gnorm(C_q, C_p)
            mask = (np.absolute(gc * points[p][1])) > loop_label_strength[max_x:min_x, max_y:min_y, 2]
            mask_bool = mask * C_q
            if np.any(mask_bool):
                # print 'match'
                for (i, j) in np.ndindex(mask_bool.shape[:2]):
                    if np.any(mask_bool[i, j]):
                        neighbor_x = i + max_x
                        neighbor_y = j + max_y
                        if loop_label_strength[neighbor_x, neighbor_y, 0, 0] < iterations:
                            # print 'append ', neighbor_x, neighbor_y
                            points[(neighbor_x, neighbor_y)] = (
                                points[p][0], points[p][1]*gc[i, j])
                            loop_label_strength[neighbor_x, neighbor_y, 0] += 1
                            loop_label_strength[neighbor_x, neighbor_y, 1] = points[p][0]
                            loop_label_strength[neighbor_x, neighbor_y, 2] = points[p][1]*gc[i, j]
            del points[p]
        if not points:
            break
    return loop_label_strength[:, :, 1] * img


def norm(point, neigh):
    rest = (point.astype(int) - neigh.astype(int)).astype(int)
    return 1 - np.sqrt(np.sum(rest ** 2)) / (sqrt(3)*255)


def addIf(x, y, neigh, points, width, height, loop):
    if 0 <=x and x < width and 0 <= y and y < height:
        if loop[x, y] == 0:
            points.append((x,y))
            loop[x,y] += 1
        else:
            neigh.append((x,y))


def add_neighbours(x, y, neigh, points, width, height, loop):
    addIf(x-1,y, neigh, points, width, height, loop)
    addIf(x-1,y-1, neigh, points, width, height, loop)
    addIf(x-1,y+1, neigh, points, width, height, loop)
    addIf(x+1,y, neigh, points, width, height, loop)
    addIf(x+1,y-1, neigh, points, width, height, loop)
    addIf(x+1,y+1, neigh, points, width, height, loop)
    addIf(x,y-1, neigh, points, width, height, loop)
    addIf(x,y+1, neigh, points, width, height, loop)


def growcut3(img, foreground, background, iterations=10):
    loop = np.zeros((img.shape[0], img.shape[1]))
    strength = np.zeros((img.shape[0], img.shape[1]))
    for p in foreground:
        strength[p] = 1
        loop[p] = 1
    for p in background:
        strength[p] = -1
        loop[p] = 1
    points = foreground + background
    for p in points:
        neighbours = list()
        add_neighbours(p[0], p[1], neighbours, points, img.shape[0], img.shape[1], loop)
        for neigh in neighbours:
            gc = norm(img[p], img[neigh])
            if np.any(gc * np.abs(strength[neigh]) > strength[p]):
                strength[p] = gc*strength[neigh]
            elif np.any(np.abs(gc*strength[p]) > np.abs(strength[neigh])):
                if strength[p]*strength[neigh] < 0 and loop[neigh] < iterations:
                    points.append(neigh)
                    loop[neigh] += 1
            neighbours.remove(neigh)
        points.remove(p)

    return strength > 0

    # loop = Matrix<int>(height, width, 0);
    # // Loop while it remains points to compute
    # while ((!listPoint.empty())) {
    #     pair<int, int> point = listPoint.front();
    #
    #     int point_x = point.first;
    #     int point_y = point.second;
    #
    #     addNeighbour(point_x,point_y);
    #
    #     // Compute the strength of the current point
    #     while (! listNeight.empty()) {
    #         pair<int, int> neight = listNeight.front();
    #
    #         int neight_x = neight.first;
    #         int neight_y = neight.second;
    #
    #         float n = norm(point_x, point_y, neight_x, neight_y);
    #
    #         if (n*(fabs(strength(neight_x, neight_y))) > fabs(strength(point_x, point_y))) {
    #             strength.change(point_x, point_y, n*strength(neight_x, neight_y));
    #         } else if (fabs(n*(strength(point_x, point_y))) > fabs(strength(neight_x, neight_y))) {
    #             if (strength(point_x, point_y)*strength(neight_x, neight_y) < 0 && loop(neight_x, neight_y) < iteration) {
    #                 listPoint.push_back(make_pair(neight_x, neight_y));
    #                 loop.increase(neight_x, neight_y);
    #             }
    #         }
    #         listNeight.pop_front();
    #     }
    #     listPoint.pop_front();
    # }
    # return strength.getData();
