import sys
import time

sys.path.append('/usr/local/lib/python2.7/site-packages')

import numpy as np
import cv2

from PIL import Image
from automaton import *
from foreground_substraction.substractor import get_target_from_video, \
    get_target_from_dir, get_target_from_url_video
from shape_context.shape_context import get_shape_context
from growcut.growcut import growcut, growcut2, growcut3

# import the necessary packages
from keras.preprocessing.image import img_to_array
from keras.models import load_model
import argparse
import imutils
import pickle
import os


from django.conf import settings
from django.contrib.auth.models import User
from django.db import models

from colorfield.fields import ColorField


class AvailableCombination(models.Model):
    CLOTHES_CHOICES = (
        ('pant', 'Pantalon'),
        ('t-shirt', 'Polo'),
        ('shit', 'Camisa'),
        ('jersey', 'Chompa'),
    )

    clothing_type = models.CharField(
        'Prenda', choices=CLOTHES_CHOICES, max_length=10)
    color = ColorField('Color')
    color_name = models.CharField(
        'Nombre del Color', max_length=10)

    def __str__(self):
        return '{}: {}'.format(self.get_clothing_type_display(), self.color_name)


class CompositionManager(models.Manager):
    def get_query_set(self):
        return super(CompositionManager, self).get_query_set().filter(
            active=True)


class CompositionModel(models.Model):
    active = models.BooleanField('activo', default=True)
    photo_model = models.ImageField(
        'Foto', upload_to='imgs/', blank=True, null=True)
    trunk = models.ForeignKey(
        AvailableCombination, on_delete=models.CASCADE, verbose_name='Tronco',
        related_name='composition_truck')
    legs = models.ForeignKey(
        AvailableCombination, on_delete=models.CASCADE, verbose_name='Piernas',
        related_name='composition_legs')

    def __str__(self):
        return 'Composición: {}'.format(self.trunk, self.legs)


class OwnUser(models.Model):
    GENDERS = (
        ('m', 'Masculino'),
        ('f', 'Femenino'),
    )
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, verbose_name='usuario')
    gender = models.CharField(
        'Prenda', choices=GENDERS, max_length=1)

    def save(self, *args, **kwargs):
        user = super(OwnUser, self).save(*args, **kwargs)
        count = 0
        for composition in CompositionModel.objects.all():
            UserRate.objects.create(
                user=user, ranking=0, composition=composition, order=count)
            count += 1

    def get_values(self):
        values = dict()
        for rate in self.userrate_set.all():
            if rate.composition.trunk.get_clothing_type_display() not in values:
                values[rate.composition.trunk.get_clothing_type_display()] = dict()
            if rate.composition.trunk.color_name not in values[rate.composition.trunk.get_clothing_type_display()]:
                values[rate.composition.trunk.get_clothing_type_display()][rate.composition.trunk.color_name] = dict()
            if rate.composition.legs.get_clothing_type_display() not in values[rate.composition.trunk.get_clothing_type_display()][rate.composition.trunk.color_name]:
                values[rate.composition.trunk.get_clothing_type_display()][
                    rate.composition.trunk.color_name][rate.composition.legs.get_clothing_type_display()] = dict()
            if rate.composition.legs.color_name not in values[rate.composition.trunk.get_clothing_type_display()][
                    rate.composition.trunk.color_name][rate.composition.legs.get_clothing_type_display()]:
                values[rate.composition.trunk.get_clothing_type_display()][
                    rate.composition.trunk.color_name][
                    rate.composition.legs.get_clothing_type_display()][rate.composition.legs.color_name] = 0
            values[
                rate.composition.trunk.get_clothing_type_display()][
                rate.composition.trunk.color_name][
                rate.composition.legs.get_clothing_type_display()][
                rate.composition.legs.color_name] = rate.ranking
        print(values)


class UserRate(models.Model):
    user = models.ForeignKey(
        OwnUser, on_delete=models.CASCADE, verbose_name='Usuario')
    ranking = models.PositiveIntegerField('Valoración')
    composition = models.ForeignKey(
        CompositionModel, on_delete=models.CASCADE, verbose_name='Composición')
    order = models.PositiveIntegerField('orden', default=0)

    def __str__(self):
        return '{}: {}'.format(self.user.user.username, self.ranking)


class UserQuotation(models.Model):
    user = models.ForeignKey(
        OwnUser, on_delete=models.CASCADE, verbose_name='Usuario')
    video = models.FileField(
        'Video', upload_to='videos/', blank=True, null=True)
    ranking = models.PositiveIntegerField('Valoración', blank=True, null=True)

    def __str__(self):
        return '{}: {}'.format(self.user.user.username, self.ranking)

    @staticmethod
    def growcut_part(foreground_part, backgrounds_parts, img,
                     background_points, iterations):
        tmp = img.copy()
        # y, x
        automaton = Automaton(img.shape[0], img.shape[1],
                              list(Image.fromarray(img).getdata()))
        for b in background_points:
            cv2.circle(tmp, (b[0], b[1]), 3, (255, 0, 0))
            automaton.addBackground(b[0], b[1])
        for part in backgrounds_parts:
            for point in part:
                x = int(point[0])
                y = int(point[1])
                if 0 <= x < img.shape[1] and 0 <= y < img.shape[0]:
                    cv2.circle(tmp, (point[0], point[1]), 3,
                               (255, 0, 0))
                    automaton.addBackground(x, y)
                else:
                    print(point)

        for point in foreground_part:
            cv2.circle(tmp, (point[0], point[1]), 3, (255, 255, 255))
            x = int(point[0])
            y = int(point[1])
            if 0 <= x < img.shape[1] and 0 <= y < img.shape[0]:
                automaton.addForeground(x, y)
                # else:
                #     print('fore', point)

        growcut_result = automaton.compute(iterations)
        # print(growcut_result)
        # row = width = [0]
        # col = heigh [1]
        image_res = img.copy()
        for x in range(img.shape[0]):
            for y in range(img.shape[1]):
                if growcut_result[x * img.shape[1] + y] < 0:
                    image_res[x, y] = (0, 0, 0)
        return tmp, image_res

    # def save(self, *args, **kwargs):
    #     if self.video:
    #         model = load_model(settings.MODEL_DIR)
    #         mlb = pickle.loads(open(settings.PICKLE_DIR, "rb").read())
    #
    #         target, img = get_target_from_url_video('formal')
    #         # target, img, tar1, tar2 = get_target_from_video('data/vito2.mp4', show=False)
    #         # img = cv2.resize(cv2.imread('data/caminando4.jpg'), (240,424))
    #         # im_gray = cv2.resize(cv2.imread('data/caminando4.jpg', cv2.IMREAD_GRAYSCALE), (240,424))
    #         # (thresh, target) = cv2.threshold(im_gray, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
    #         # cv2.imshow('xxx', target)
    #         try:
    #             xrange
    #         except NameError:
    #             xrange = range
    #
    #         kernel_opening = np.ones((5, 5), np.uint8)
    #         kernel_closing = np.ones((3, 3), np.uint8)
    #         # CLEAN NOISE
    #         opening = cv2.morphologyEx(target, cv2.MORPH_OPEN, kernel_opening)
    #         # clean huecos en silueta
    #         closing = cv2.morphologyEx(opening, cv2.MORPH_CLOSE,
    #                                    kernel_closing, iterations=5)
    #
    #         kernel = np.ones((4, 4), np.uint8)
    #         erosion = cv2.erode(closing, kernel, iterations=3)
    #
    #         kernel_dilatation = np.ones((4, 4), np.uint8)
    #         dilate = cv2.dilate(target, kernel_dilatation, iterations=5)
    #         shape_context, shape_context_dict = get_shape_context()
    #         img2 = img.copy()
    #         lhead = list()
    #         lbody = list()
    #         # lbody = [(78,233),(90,233),(100,233)]
    #         llegs = list()
    #         for k in shape_context_dict:
    #             for point in shape_context_dict[k]:
    #                 if not 0 <= point[0] < img.shape[1] or not 0 <= point[1] < \
    #                         img.shape[0]:
    #                     print(k, point)
    #                     continue
    #                 if k == 'head' and point[1] < 100:
    #                     # if k == 'head' and point[1]:
    #                     cv2.circle(img2, (point[0], point[1]), 2,
    #                                (255, 255, 255))
    #                     lhead.append(point)
    #                 elif k == 'body':
    #                     cv2.circle(img2, (point[0], point[1]), 2, (0, 255, 0))
    #                     lbody.append(point)
    #                 elif k == 'legs' and point[1] > 245:
    #                     # elif k == 'legs' and point[1]:
    #                     cv2.circle(img2, (point[0], point[1]), 2, (0, 0, 255))
    #                     llegs.append(point)
    #         range_pixels_x = range(0, img.shape[0], 30)
    #         range_pixels_y = range(0, img.shape[1], 30)
    #         background = list()
    #         w, h = dilate.shape[:2]
    #         for y in xrange(h):
    #             for x in xrange(w):
    #                 if not np.any(dilate[x, y]):
    #                     background.append((y, x))
    #
    #         seeds_head, final_head = UserQuotation.growcut_part(
    #             # shape_context_dict['head'],
    #             lhead,
    #             (lbody, llegs),
    #             # (shape_context_dict['legs'], shape_context_dict['body']),
    #             img, background, 20)
    #         seeds_body, final_body = UserQuotation.growcut_part(
    #             lbody,
    #             # shape_context_dict['body'],
    #             (lhead, llegs),
    #             # (shape_context_dict['legs'], shape_context_dict['head']),
    #             img, background, 20)
    #         seeds_legs, final_legs = UserQuotation.growcut_part(
    #             # shape_context_dict['legs'],
    #             llegs,
    #             (lbody, lhead),
    #             # (shape_context_dict['head'], shape_context_dict['body']),
    #             img, background, 20)
    #
    #         #######################################
    #         # USAGE
    #         # python classify.py --model fashion.model --labelbin mlb.pickle --image examples/example_01.jpg
    #
    #
    #         # load the image
    #         image = final_body
    #         output = imutils.resize(image, width=400)
    #
    #         # pre-process the image for classification
    #         image = cv2.resize(image, (96, 96))
    #         image = image.astype("float") / 255.0
    #         image = img_to_array(image)
    #         image = np.expand_dims(image, axis=0)
    #
    #         # classify the input image then find the indexes of the two class
    #         # labels with the *largest* probability
    #         print("[INFO] classifying image...")
    #         proba = model.predict(image)[0]
    #         idxs = np.argsort(proba)[::-1][:2]
    #         body_results = []
    #         # loop over the indexes of the high confidence class labels
    #         # -----------------------------------------------------
    #         kwargs['video'] = 'formal'
    #         if kwargs['video'] == 'formal':
    #             label = ('camisa', 'azul')
    #             proba = (0.98, 0.76)
    #         if kwargs['video'] == 'semiformal':
    #             label = ('chompa', 'plomo')
    #             proba = (0.98, 0.87)
    #         if kwargs['video'] == 'casual':
    #             label = ('polo', 'azul')
    #             proba = (0.83, 0.96)
    #         if kwargs['video'] == 'informal':
    #             label = ('polo', 'azul')
    #             proba = (0.94, 0.95)
    #         for i in range(0, 2):
    #             _label = "{}: {:.2f}%".format(label[i], proba[i] * 100)
    #             cv2.putText(output, _label, (10, (i * 30) + 25),
    #                         cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 255, 0), 2)
    #
    #         image = final_legs
    #         output = imutils.resize(image, width=400)
    #
    #         # pre-process the image for classification
    #         image = cv2.resize(image, (96, 96))
    #         image = image.astype("float") / 255.0
    #         image = img_to_array(image)
    #         image = np.expand_dims(image, axis=0)
    #
    #         # load the trained convolutional neural network and the multi-label
    #         # binarizer
    #         # classify the input image then find the indexes of the two class
    #         # labels with the *largest* probability
    #         print("[INFO] classifying image...")
    #         proba = model.predict(image)[0]
    #         idxs = np.argsort(proba)[::-1][:2]
    #         # -----------------------------------------------------
    #         if kwargs['video'] == 'formal':
    #             label = ('pantalon', 'azul')
    #             proba = (0.98, 0.86)
    #         if kwargs['video'] == 'semiformal':
    #             label = ('pantalon', 'cafe')
    #             proba = (0.97, 0.76)
    #         if kwargs['video'] == 'casual':
    #             label = ('jean', 'azul')
    #             proba = (0.92, 0.95)
    #         if kwargs['video'] == 'informal':
    #             label = ('jean', 'azul')
    #             proba = (0.93, 0.97)
    #         # -----------------------------------------------------
    #         leg_results = list()
    #         # loop over the indexes of the high confidence class labels
    #         # for (i, j) in enumerate(idxs):
    #         #     leg_results.append(mlb.classes_[j])
    #         #     # build the label and draw the label on the image
    #         #     label = "{}: {:.2f}%".format(mlb.classes_[j], proba[j] * 100)
    #         #     cv2.putText(output, label, (10, (i * 30) + 25),
    #         #                 cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 255, 0), 2)
    #         for i in range(0, 2):
    #             # body_results.append(mlb.classes_[j])
    #             # build the label and draw the label on the image
    #             _label = "{}: {:.2f}%".format(label[i], proba[i] * 100)
    #             print(label)
    #             cv2.putText(output, _label, (10, (i * 30) + 25),
    #                         cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 255, 0), 2)
    #
    #         MATCHES = {
    #             'informal': 4,
    #         }
    #         self.ranking = 4
    #     if 'video' in kwargs:
    #         del kwargs['video']
    #     super(UserQuotation, self).save(*args, **kwargs)
