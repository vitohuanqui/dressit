from django.forms import ModelForm

from .models import UserRate, UserQuotation


class UserRateForm(ModelForm):
    class Meta:
        model = UserRate
        fields = ['ranking', ]


class UserQuotationForm(ModelForm):
    class Meta:
        model = UserQuotation
        fields = ['video']

    def __init__(self, *args, **kwargs):
        self._user = kwargs.pop('user')
        super(UserQuotationForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        inst = super(UserQuotationForm, self).save(commit=False)
        inst.user = self._user
        if commit:
            inst.save()
            self.save_m2m()
        return inst
