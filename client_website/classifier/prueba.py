# import math
# import cv2
# import numpy as np
# origin = [2, 3]
# refvec = [0, 1]
#
# from scipy.spatial import distance
#
# def sortpts_clockwise(A):
#     # Sort A based on Y(col-2) coordinates
#     sortedAc2 = A[np.argsort(A[:,1]),:]
#
#     # Get top two and bottom two points
#     top2 = sortedAc2[0:2,:]
#     bottom2 = sortedAc2[2:,:]
#
#     # Sort top2 points to have the first row as the top-left one
#     sortedtop2c1 = top2[np.argsort(top2[:,0]),:]
#     top_left = sortedtop2c1[0,:]
#
#     # Use top left point as pivot & calculate sq-euclidean dist against
#     # bottom2 points & thus get bottom-right, bottom-left sequentially
#     sqdists = distance.cdist(top_left[None], bottom2, 'sqeuclidean')
#     rest2 = bottom2[np.argsort(np.max(sqdists,0))[::-1],:]
#
#     # Concatenate all these points for the final output
#     return np.concatenate((sortedtop2c1,rest2),axis =0)
#
# def clockwiseangle_and_distance(point):
#     # Vector between point and the origin: v = p - o
#     vector = [point[0]-origin[0], point[1]-origin[1]]
#     # Length of vector: ||v||
#     lenvector = math.hypot(vector[0], vector[1])
#     # If length is zero there is no angle
#     if lenvector == 0:
#         return -math.pi, 0
#     # Normalize vector: v/||v||
#     normalized = [vector[0]/lenvector, vector[1]/lenvector]
#     dotprod  = normalized[0]*refvec[0] + normalized[1]*refvec[1]     # x1*x2 + y1*y2
#     diffprod = refvec[1]*normalized[0] - refvec[0]*normalized[1]     # x1*y2 - y1*x2
#     angle = math.atan2(diffprod, dotprod)
#     # Negative angles represent counter-clockwise angles so we need to subtract them
#     # from 2*pi (360 degrees)
#     if angle < 0:
#         return 2*math.pi+angle, lenvector
#     # I return first the angle because that's the primary sorting criterium
#     # but if two vectors have the same angle then the shorter distance should come first.
#     return angle, lenvector
#
# src = "data/erosion.jpg"
# src = cv2.imread(src, 0).transpose()
# treshold=50
# dst = cv2.Canny(src, threshold1=treshold, threshold2=treshold * 3, apertureSize=3)
# w,h = src.shape[:2]
# b = np.zeros((w,h,3), np.uint8)
# b2 = np.zeros((w,h,3), np.uint8)
# print(w,h)
# points = list()
# for y in range(h):
#     for x in range(w):
#         try:
#             c = dst[x, y]
#             if c == 255:
#                 points.append((x, y))
#         except:
#             print(x, y)
# origin = points[0]
# points = sorted(points, key=clockwiseangle_and_distance)
# # points = sortpts_clockwise(np.array(points))
# count = 0
# cuantos= 0
# step = int(len(points) / 100)
#
# for p in points:
#     if not count % step:
#         b[p] = (255,255,255)
#         cuantos +=1
#     count += 1
#
# cv2.imshow("sin nada", b)
# count = 0
# for p in points:
#     if not count % 4:
#         b2[p] = (255,255,255)
#     count +=1
#
# cv2.imshow("aca pe",b2)
#
#
# cv2.waitKey(0)
# cv2.destroyAllWindows()
# print(step)
# print(cuantos)

import cv2
import numpy as np
a = [(151, 0), (152, 303), (154, 310), (154, 187), (159, 307), (162, 298), (158, 182), (159, 192), (164, 283), (165, 281), (165, 275), (165, 269), (164, 232), (165, 238), (174, 296), (174, 285), (176, 279), (181, 264), (181, 236), (183, 245), (181, 219), (184, 204), (186, 189), (188, 174), (182, 133), (187, 152), (187, 149), (182, 114), (201, 172), (189, 127), (187, 119), (204, 173), (208, 183), (186, 111), (208, 178), (200, 150), (185, 102), (213, 176), (219, 179), (217, 170), (218, 167), (213, 152), (218, 161), (208, 135), (206, 125), (206, 121), (200, 104), (198, 92), (198, 83), (170, 30), (198, 67), (191, 54), (174, 22), (171, 9), (163, 1), (143, 5), (140, 14), (119, 56), (115, 66), (114, 76), (114, 86), (114, 95), (107, 119), (108, 118), (92, 164), (96, 157), (104, 137), (134, 51), (96, 168), (97, 174), (99, 180), (101, 182), (104, 180), (111, 157), (111, 161), (111, 164), (118, 140), (111, 174), (111, 178), (124, 124), (129, 107), (130, 117), (126, 145), (130, 124), (130, 128), (124, 172), (125, 182), (127, 197), (130, 212), (129, 245), (130, 240), (132, 233), (134, 272), (137, 281), (138, 288), (142, 311), (147, 243), (147, 273), (147, 312), (148, 262)]
b = np.zeros((500,500,3), np.uint8)
cabeza = list()
pecho= list()
piernas= list()
for p in a:
    if p[1] < 45:
        b[p] = (255,255,255)
        cabeza.append(p)
    elif not 124 <= p[0] <= 192:
        b[p] = (255,0,0)
        pecho.append(p)
    elif 45 <= p[1] <= 147:
        b[p] = (255,0,0)
        pecho.append(p)
    elif 188 <= p[1]:
        b[p] = (0,0,255)
        piernas.append(p)
cv2.imshow("asd", b)
cv2.waitKey(0)
cv2.destroyAllWindows()

print((pecho))
print((cabeza))
print((piernas))