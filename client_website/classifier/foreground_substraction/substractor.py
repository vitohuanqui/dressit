import numpy as np
import cv2
from os import listdir

def get_target_from_video(video, show=False):

    cap = cv2.VideoCapture(video)
    fgbg = cv2.bgsegm.createBackgroundSubtractorCNT()
    fgbg1 = cv2.bgsegm.createBackgroundSubtractorGMG()
    fgbg2 = cv2.bgsegm.createBackgroundSubtractorMOG()
    frames = list()
    i=0
    while True:
        ret, frame = cap.read()
        if not ret:
            break
        frame = cv2.resize(frame, (0, 0), fx=0.5, fy=0.5)
        frames.append(frame)
        cv2.imwrite('data/background/{0}.jpg'.format(i), frame)
        cv2.imwrite('data/background/{0}.jpg'.format(i+1), frame)
        cv2.imwrite('data/background/{0}.jpg'.format(i+2), frame)
        i += 3
        fgmask = fgbg.apply(frame)
        if show:
            cv2.imshow('frame', frame)
            cv2.imshow('frame1', fgmask)
    cap.release()
    if show:
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    return fgbg.apply(frames[len(frames)-1]), frames[len(frames)-1]


def get_target_from_dir(show=False):

    background_files = listdir('data/background')
    fgbg = cv2.createBackgroundSubtractorMOG2(history=len(background_files), varThreshold=1000, detectShadows=0)
    for file in background_files:
        background_img = cv2.imread("data/background/{0}".format(file), 0)
        fgbg.apply(background_img)
    image = cv2.imread('data/foreground/164.jpg', 0)
    img = cv2.imread('data/foreground/164.jpg')

    return fgbg.apply(image), img

def get_target_from_url_video(video):
    PARAMS = {
        'formal': 324,
        'semiformal': 304,
        'casual': 301,
        'informal': 278,
    }
    umbra_PARAMS = {
        'formal': 80,
        'semiformal': 20,
        'casual': 200,
        'informal': 80,
    }
    background_files = cv2.VideoCapture('./data/{}.mp4'.format(video))
    fgbg = cv2.createBackgroundSubtractorMOG2(history=200, varThreshold=umbra_PARAMS[video], detectShadows=0)
    count = 0
    param = PARAMS[video]
    while (True):
        count +=1
        ret, frame = background_files.read()
        if count < 200:
            fgbg.apply(frame)
        elif count == param:
            return fgbg.apply(frame), frame
