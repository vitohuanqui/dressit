from django.urls import path

from .views import index, classifier, classify_video

urlpatterns = [
    path('', index, name='index'),
    path('classify', classify_video, name='classify_video'),
    path('classifier/<slug:option>/<int:order>/', classifier, name='classifier'),
]