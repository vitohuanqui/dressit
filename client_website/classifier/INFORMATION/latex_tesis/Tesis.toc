\select@language {spanish}
\contentsline {chapter}{\numberline {1}Introducci�n}{2}
\contentsline {section}{\numberline {1.1}Motivaci�n y Contexto}{3}
\contentsline {section}{\numberline {1.2}Planteamiento del Problema}{4}
\contentsline {section}{\numberline {1.3}Objetivos}{4}
\contentsline {subsection}{\numberline {1.3.1}Objetivos Espec�ficos}{4}
\contentsline {section}{\numberline {1.4}Organizaci�n de la tesis}{4}
\contentsline {chapter}{\numberline {2}Marco Te�rico}{6}
\contentsline {section}{\numberline {2.1}Sustracci�n de Fondo}{6}
\contentsline {section}{\numberline {2.2}Operaciones Morfol�gicas}{6}
\contentsline {subsection}{\numberline {2.2.1}Dilataci�n Binaria}{7}
\contentsline {subsubsection}{Propiedades}{7}
\contentsline {subsection}{\numberline {2.2.2}Erosi�n Binaria}{8}
\contentsline {subsubsection}{Propiedades}{8}
\contentsline {subsection}{\numberline {2.2.3}Apertura}{8}
\contentsline {subsubsection}{Teorema de Caracterizaci�n}{9}
\contentsline {subsection}{\numberline {2.2.4}Clausura}{9}
\contentsline {subsubsection}{Propiedades}{9}
\contentsline {section}{\numberline {2.3}Shape Context}{9}
\contentsline {subsection}{\numberline {2.3.1}Caracter�sticas}{10}
\contentsline {subsection}{\numberline {2.3.2}Algoritmo H�ngaro}{10}
\contentsline {subsection}{\numberline {2.3.3}Segmentaci�n de Im�genes (Cuerpo))}{11}
\contentsline {section}{\numberline {2.4}Aut�mata Celular}{11}
\contentsline {subsubsection}{Elementos}{12}
\contentsline {subsection}{\numberline {2.4.1}GrowCut}{12}
\contentsline {section}{\numberline {2.5}\textit {Convolutional Neural Networks (CNNs / ConvNets)}}{13}
\contentsline {subsubsection}{Descripci�n de la Arquitectura}{13}
\contentsline {subsubsection}{Capas}{13}
\contentsline {subsection}{\numberline {2.5.1}\textit {TensorFlow}}{14}
\contentsline {subsection}{\numberline {2.5.2}\textit {Red convolucional VGGNet}}{14}
\contentsline {section}{\numberline {2.6}Estado del Arte}{14}
\contentsline {subsection}{\numberline {2.6.1}Sustracci�n de \textit {Background} - Detecci�n de \textit {ForeGround}}{14}
\contentsline {subsection}{\numberline {2.6.2}Detecci�n y Segmentaci�n de la Silueta}{17}
\contentsline {subsection}{\numberline {2.6.3}Reconocimiento de Caracter�sticas Espec�ficas}{19}
\contentsline {chapter}{\numberline {3}Propuesta}{21}
\contentsline {chapter}{\numberline {4}Pruebas y Resultados}{26}
\contentsline {section}{\numberline {4.1}Conjunto de Datos}{26}
\contentsline {subsection}{\numberline {4.1.1}Datos Enfocados en YOLO}{26}
\contentsline {subsection}{\numberline {4.1.2}Datos Enfocados en \textit {Shape Context}}{27}
\contentsline {subsection}{\numberline {4.1.3}Datos Enfocados en \textit {CNN VGGNet}}{27}
\contentsline {subsection}{\numberline {4.1.4}Datos Enfocados en el Proceso Completo}{27}
\contentsline {section}{\numberline {4.2}Dise�o del Experimento}{28}
\contentsline {subsection}{\numberline {4.2.1}Participantes}{28}
\contentsline {subsection}{\numberline {4.2.2}Dise�o de la Propuesta}{28}
\contentsline {section}{\numberline {4.3}Evaluaci�n del Programa Completo}{30}
\contentsline {chapter}{\numberline {5}Conclusiones y Trabajos Futuros}{34}
\contentsline {section}{\numberline {5.1}Trabajos Futuros}{35}
\contentsline {chapter}{Bibliograf�a}{40}
