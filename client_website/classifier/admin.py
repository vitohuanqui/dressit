from django.contrib import admin

from .models import AvailableCombination, CompositionModel, UserRate, \
    UserQuotation


@admin.register(AvailableCombination)
class AvailableCombinationAdmin(admin.ModelAdmin):
    pass


@admin.register(CompositionModel)
class CompositionModelAdmin(admin.ModelAdmin):
    pass


@admin.register(UserRate)
class UserRateAdmin(admin.ModelAdmin):
    pass

@admin.register(UserQuotation)
class UserQuotationAdmin(admin.ModelAdmin):
    pass