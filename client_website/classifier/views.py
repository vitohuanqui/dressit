from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect

from .forms import UserRateForm, UserQuotationForm
from .models import CompositionModel


@login_required
def index(request):
    is_complete = 0
    filled = True
    count = step = 0
    for rate in request.user.ownuser.userrate_set.all():
        is_complete += rate.ranking
        if not rate.ranking:
            step = count
            filled = False
        count += 1
    if not filled and is_complete:
        is_complete = 1
    elif filled and is_complete:
        is_complete = 2
    return render(request, 'index.html', dict(user=request.user, is_filled=is_complete, step=step))


@login_required
def classifier(request, option, order):
    if option == 'reset':
        for rate in request.user.ownuser.userrate_set.all():
            rate.ranking = 0
            rate.save()
    rate = request.user.ownuser.userrate_set.get(order=int(order))
    if request.method == 'POST':
        form = UserRateForm(request.POST, instance=rate)
        if form.is_valid():
            form.save()
            if order == request.user.ownuser.userrate_set.all().count() - 1:
                return redirect('index')
            else:
                return redirect('classifier', option='fill', order=order + 1)
    else:
        form = UserRateForm(instance=rate)
    return render(request, 'classifier/index.html', dict(form=form, composition=rate.composition))


@login_required
def classify_video(request):
    print('v9sta classify', request.method)
    if request.method == 'POST':
        print('se va a grabar')
        form = UserQuotationForm(request.POST, request.FILES, user=request.user.ownuser)
        if form.is_valid():
            print('se grabo')
            form.save()
            return redirect('index')
    else:
        form = UserQuotationForm(user=request.user.ownuser)
    return render(request, 'classifier/video.html', dict(form=form))
