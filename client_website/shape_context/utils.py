try:
    xrange
except NameError:
    xrange = range


from math import sin, cos, sqrt, pi
import cv2
import time
import math
from numpy import *

CANNY = 1

def dist2(x,c):
    """
        Euclidian distance matrix
    """
    ncentres = c.shape[0]
    ndata = x.shape[0]
    return (ones((ncentres, 1)) * (((power(x,2)).H)).sum(axis=0)).H + ones((ndata, 1)) * ((power(c,2)).H).sum(axis=0) - multiply(2,(x*(c.H)));





def get_points_from_img(src, treshold=50, simpleto=100, t=CANNY, show_img=False):
    origin = [0,0]
    refvec = [0,1]

    def clockwiseangle_and_distance(point):
        # Vector between point and the origin: v = p - o
        vector = [point[0] - origin[0], point[1] - origin[1]]
        # Length of vector: ||v||
        lenvector = math.hypot(vector[0], vector[1])
        # If length is zero there is no angle
        if lenvector == 0:
            return -math.pi, 0
        # Normalize vector: v/||v||
        normalized = [vector[0] / lenvector, vector[1] / lenvector]
        dotprod = normalized[0] * refvec[0] + normalized[1] * refvec[
            1]  # x1*x2 + y1*y2
        diffprod = refvec[1] * normalized[0] - refvec[0] * normalized[
            1]  # x1*y2 - y1*x2
        angle = math.atan2(diffprod, dotprod)
        # Negative angles represent counter-clockwise angles so we need to subtract them
        # from 2*pi (360 degrees)
        if angle < 0:
            return 2 * math.pi + angle, lenvector
        # I return first the angle because that's the primary sorting criterium
        # but if two vectors have the same angle then the shorter distance should come first.
        return angle, lenvector

    if isinstance(src, str):
        src = cv2.imread(src, 0).transpose()
    if t == CANNY:
        dst = cv2.Canny(src, threshold1=treshold, threshold2=treshold * 3, apertureSize=3)
    A = src.copy()

    px, py = gradient(A)
    points = []
    w, h = src.shape[:2]
    for y in xrange(h):
        for x in xrange(w):
            try:
                c = dst[x, y]
                if c == 255:
                    points.append((x, y))
            except:
                print(x, y)
    origin = points[0]
    points = sorted(points, key=clockwiseangle_and_distance)
    # points = sortpts_clockwise(np.array(points))
    count = 0
    amount = 0
    step = int(len(points) / 100)
    new_points = list()
    b = zeros((w,h,3), uint8)
    for p in points:
        if amount == simpleto:
            break
        if not count % step:
            b[p] = (255,255,255)
            new_points.append(p)
            amount += 1
        count += 1
    r = 2
    if show_img:
        cv2.imshow("sin nada", b)
    else:
        cv2.imshow("sxxxin nada", b)

    points = new_points
    switch = True
    while len(points) > simpleto:
        newpoints = points
        xr = range(0, w, r)
        yr = range(0, h, r)
        if switch:
            switch = False
            for p in points:
                if p[0] not in xr and p[1] not in yr:
                    newpoints.remove(p)
                    if len(points) <= simpleto:
                        break
        else:
            switch = True
            for p in reversed(points):
                if p[0] not in xr and p[1] not in yr:
                    newpoints.remove(p)
                    if len(points) <= simpleto:
                        break
        r += 1
    T = zeros((simpleto, 1))
    for i, (x, y) in enumerate(points):
        T[i] = math.atan2(py[x, y], px[x, y]) + pi / 2;

    return points, asmatrix(T)


def bookenstain(X, Y, beta):
    """
        Bookstein PAMI89

        Article: Principal Warps: Thin-Plate Splines and the Decomposition of Deformations

    """
    X = asmatrix(X)
    Y = asmatrix(Y)

    N = X.shape[0]
    r2 = dist2(X, X)
    K = multiply(r2, log(r2 + eye(N, N)))
    P = concatenate((ones((N, 1)), X), 1)
    L = bmat([[K, P], [P.H, zeros((3, 3))]])
    V = concatenate((Y.H, zeros((2, 3))), 1)

    L[0:N, 0:N] = L[0:N, 0:N] + beta * eye(N, N)

    invL = linalg.inv(L)

    # L^-1 * v^T = (W | a_1 a_x a_y)^T
    c = invL * (V.H)
    cx = c[:, 0]
    cy = c[:, 1]

    Q = (c[0:N, :].H) * K * c[0:N, :]
    E = mean(diag(Q))

    n_good = 10

    A = concatenate(
        (cx[n_good + 2:n_good + 3, :], cy[n_good + 2:n_good + 3, :]), 1);
    s = linalg.svd(A);
    aff_cost = log(s[0] / s[1])

    return cx, cy, E, aff_cost, L