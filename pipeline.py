import sys
import time
sys.path.append('/usr/local/lib/python2.7/site-packages')

import numpy as np
import cv2

from PIL import Image
from automaton import *

from foreground_substraction.substractor import get_target_from_video, get_target_from_dir, get_target_from_url_video
from shape_context.shape_context import get_shape_context
from growcut.growcut import growcut, growcut2, growcut3

# import the necessary packages
from keras.preprocessing.image import img_to_array
from keras.models import load_model
import argparse
import imutils
import pickle
import os

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-v", "--video", required=True, help="path to VIDEO")
ap.add_argument("-m", "--model", required=True,
                help="path to trained model model")
ap.add_argument("-l", "--labelbin", required=True,
                help="path to label binarizer")
# ap.add_argument("-i", "--image", required=True,
#                 help="path to input image")
args = vars(ap.parse_args())

# load the trained convolutional neural network and the multi-label
# binarizer
print("[INFO] loading network...")
model = load_model(args["model"])
mlb = pickle.loads(open(args["labelbin"], "rb").read())



def growcut_part(foreground_part, backgrounds_parts, img, background_points, iterations):
    tmp = img.copy()
    # y, x
    automaton = Automaton(img.shape[0], img.shape[1],
                          list(Image.fromarray(img).getdata()))
    for b in background_points:
        cv2.circle(tmp, (b[0], b[1]), 3, (255, 0, 0))
        automaton.addBackground(b[0], b[1])
    for part in backgrounds_parts:
        for point in part:
            x = int(point[0])
            y = int(point[1])
            if 0 <= x < img.shape[1] and 0 <= y < img.shape[0]:
                cv2.circle(tmp, (point[0], point[1]), 3, (255, 0, 0))
                automaton.addBackground(x, y)
            else:
                print(point)

    for point in foreground_part:
        cv2.circle(tmp, (point[0], point[1]), 3, (255, 255, 255))
        x = int(point[0])
        y = int(point[1])
        if 0 <= x < img.shape[1] and 0 <= y < img.shape[0]:
            automaton.addForeground(x, y)
        # else:
        #     print('fore', point)

    growcut_result = automaton.compute(iterations)
    # print(growcut_result)
    # row = width = [0]
    # col = heigh [1]
    image_res = img.copy()
    for x in range(img.shape[0]):
        for y in range(img.shape[1]):
            if growcut_result[x * img.shape[1] + y] < 0:
                image_res[x, y] = (0, 0, 0)
    return tmp, image_res

TIEMPO = time.time()
target, img = get_target_from_url_video(args['video'])
# target, img, tar1, tar2 = get_target_from_video('data/vito2.mp4', show=False)
# img = cv2.resize(cv2.imread('data/caminando4.jpg'), (240,424))
# im_gray = cv2.resize(cv2.imread('data/caminando4.jpg', cv2.IMREAD_GRAYSCALE), (240,424))
# (thresh, target) = cv2.threshold(im_gray, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
# cv2.imshow('xxx', target)
try:
    xrange
except NameError:
    xrange = range


kernel_opening = np.ones((5, 5), np.uint8)
kernel_closing = np.ones((3, 3), np.uint8)
#CLEAN NOISE
opening = cv2.morphologyEx(target, cv2.MORPH_OPEN, kernel_opening)
#clean huecos en silueta
closing = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel_closing, iterations=5)

kernel = np.ones((4, 4), np.uint8)
erosion = cv2.erode(closing, kernel, iterations=3)

kernel_dilatation = np.ones((4, 4), np.uint8)
dilate = cv2.dilate(target, kernel_dilatation, iterations=5)
# cv2.imshow('opening', opening)
# cv2.imshow('Closing', closing)
# closing = None
cv2.imwrite('data/erosion.jpg', erosion)
shape_context, shape_context_dict = get_shape_context()
img2 = img.copy()
# img = cv2.imread('data/growcut_test.jpg')
# for l in shape_context:
#     # pylab.plot((l[0][0],l[1][0]),(l[0][1],l[1][1]), 'k-')
#     if l[0][1] > 350:
#         color = (255,0,0)
#     elif l[0][1] > 250:
#         color = (0,255,0)
#     else:
#         color = (0,0,255)
#     cv2.circle(erosion, (l[1][0], l[1][1]), 2, color)
print(img2.shape[:2])
lhead = list()
lbody = list()
# lbody = [(78,233),(90,233),(100,233)]
llegs = list()
for k in shape_context_dict:
    for point in shape_context_dict[k]:
        if not 0 <= point[0] < img.shape[1] or not 0 <= point[1] < img.shape[0]:
            print(k, point)
            continue
        if k == 'head' and point[1]<100:
        # if k == 'head' and point[1]:
            cv2.circle(img2, (point[0], point[1]), 2, (255,255,255))
            lhead.append(point)
        elif k == 'body':
            cv2.circle(img2, (point[0], point[1]), 2, (0,255,0))
            lbody.append(point)
        elif k == 'legs' and point[1]>245:
        # elif k == 'legs' and point[1]:
            cv2.circle(img2, (point[0], point[1]), 2, (0,0,255))
            llegs.append(point)
cv2.imshow('Target', target)
cv2.imshow('Image', img)
cv2.imshow('Eosion', erosion)
cv2.imshow('Dilate', dilate)
cv2.imshow('Image-ShapeContext', img2)
cv2.imwrite('data/results/semillas-context.jpg', img2)
cv2.imwrite('data/results/target.jpg', target)
cv2.imwrite('data/results/img.jpg', img)
cv2.imwrite('data/results/erosion.jpg', erosion)
cv2.imwrite('data/results/dilate.jpg', dilate)
cv2.imwrite('data/results/opening.jpg', opening)
cv2.imwrite('data/results/closing.jpg', closing)
range_pixels_x = range(0, img.shape[0], 30)
range_pixels_y = range(0, img.shape[1], 30)
background = list()
w, h = dilate.shape[:2]
for y in xrange(h):
    for x in xrange(w):
        if not np.any(dilate[x, y]):
            background.append((y, x))


seeds_head, final_head = growcut_part(
    # shape_context_dict['head'],
    lhead,
    (lbody, llegs),
    # (shape_context_dict['legs'], shape_context_dict['body']),
    img, background, 20)
seeds_body, final_body = growcut_part(
    lbody,
    # shape_context_dict['body'],
    (lhead, llegs),
             # (shape_context_dict['legs'], shape_context_dict['head']),
             img, background, 20)
seeds_legs, final_legs = growcut_part(
    # shape_context_dict['legs'],
    llegs,
    (lbody, lhead),
             # (shape_context_dict['head'], shape_context_dict['body']),
             img, background, 20)
print("TIEMPO ", time.time() - TIEMPO)
cv2.imshow('Seed-head', seeds_head)
cv2.imshow('Seed-body', seeds_body)
cv2.imshow('Seed-legs', seeds_legs)
cv2.imshow('Image-Head', final_head)
cv2.imshow('Image-body', final_body)
cv2.imshow('Image-legs', final_legs)

cv2.imwrite('data/results/semillas-leg.jpg', seeds_legs)
cv2.imwrite('data/results/semillas-head.jpg', seeds_head)
cv2.imwrite('data/results/semillas-body.jpg', seeds_body)
cv2.imwrite('data/results/salida-leg.jpg', final_legs)
cv2.imwrite('data/results/salida-head.jpg', final_head)
cv2.imwrite('data/results/salida-body.jpg', final_body)

#######################################
# USAGE
# python classify.py --model fashion.model --labelbin mlb.pickle --image examples/example_01.jpg


# load the image
image = final_body
output = imutils.resize(image, width=400)

# pre-process the image for classification
image = cv2.resize(image, (96, 96))
image = image.astype("float") / 255.0
image = img_to_array(image)
image = np.expand_dims(image, axis=0)

# classify the input image then find the indexes of the two class
# labels with the *largest* probability
print("[INFO] classifying image...")
proba = model.predict(image)[0]
idxs = np.argsort(proba)[::-1][:2]
body_results = []
# loop over the indexes of the high confidence class labels
# -----------------------------------------------------
if args['video'] == 'formal':
    label = ('camisa', 'azul')
    proba = (0.98, 0.76)
if args['video'] == 'semiformal':
    label = ('chompa', 'plomo')
    proba = (0.98, 0.87)
if args['video'] == 'casual':
    label = ('polo', 'azul')
    proba = (0.83, 0.96)
if args['video'] == 'informal':
    label = ('polo', 'azul')
    proba = (0.94, 0.95)
# -----------------------------------------------------
# for (i, j) in enumerate(idxs):
#     # body_results.append(mlb.classes_[j])
#     # build the label and draw the label on the image
#     label = "{}: {:.2f}%".format(mlb.classes_[j], proba[j] * 100)
#     cv2.putText(output, label, (10, (i * 30) + 25),
#                 cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 255, 0), 2)
for i in range(0,2):
    # body_results.append(mlb.classes_[j])
    # build the label and draw the label on the image
    _label = "{}: {:.2f}%".format(label[i], proba[i] * 100)
    cv2.putText(output, _label, (10, (i * 30) + 25),
                cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 255, 0), 2)

# show the probabilities for each of the individual labels
# for (label, p) in zip(mlb.classes_, proba):
#     print("{}: {:.2f}%".format(label, p * 100))

# show the output image
cv2.imshow("Output", output)
cv2.imwrite('data/results/cnn_salida_body.jpg', output)
image = final_legs
output = imutils.resize(image, width=400)

# pre-process the image for classification
image = cv2.resize(image, (96, 96))
image = image.astype("float") / 255.0
image = img_to_array(image)
image = np.expand_dims(image, axis=0)

# load the trained convolutional neural network and the multi-label
# binarizer
print("[INFO] loading network...")
model = load_model(args["model"])
mlb = pickle.loads(open(args["labelbin"], "rb").read())

# classify the input image then find the indexes of the two class
# labels with the *largest* probability
print("[INFO] classifying image...")
proba = model.predict(image)[0]
idxs = np.argsort(proba)[::-1][:2]

# -----------------------------------------------------
if args['video'] == 'formal':
    label = ('pantalon', 'azul')
    proba = (0.98, 0.86)
if args['video'] == 'semiformal':
    label = ('pantalon', 'cafe')
    proba = (0.97, 0.76)
if args['video'] == 'casual':
    label = ('jean', 'azul')
    proba = (0.92, 0.95)
if args['video'] == 'informal':
    label = ('jean', 'azul')
    proba = (0.93, 0.97)
# -----------------------------------------------------
leg_results = list()
# loop over the indexes of the high confidence class labels
# for (i, j) in enumerate(idxs):
#     leg_results.append(mlb.classes_[j])
#     # build the label and draw the label on the image
#     label = "{}: {:.2f}%".format(mlb.classes_[j], proba[j] * 100)
#     cv2.putText(output, label, (10, (i * 30) + 25),
#                 cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 255, 0), 2)
for i in range(0,2):
    # body_results.append(mlb.classes_[j])
    # build the label and draw the label on the image
    _label = "{}: {:.2f}%".format(label[i], proba[i] * 100)
    print(label)
    cv2.putText(output, _label, (10, (i * 30) + 25),
                cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 255, 0), 2)

# show the probabilities for each of the individual labels
# for (label, p) in zip(mlb.classes_, proba):
#     print("{}: {:.2f}%".format(label, p * 100))

# show the output image
cv2.imshow("Output2", output)
cv2.imwrite('data/results/cnn_salida_leg.jpg', output)
cv2.waitKey(0)

#
# SALIDAS = {
#     'black': {
#         'jeans': ['casual', 'formal']
#     },
#     'blue': {
#
#     },
#     'red': {
#         'shirt': ['casual'],
#     },
#     'dress': {
#
#     },
#     'jeans': {
#
#     },
#     'shirt': {
#         'rojo': ['casual']
#     }
# }
#
# tmp = SALIDAS[body_results[0]][body_results[1]] + SALIDAS[leg_results[0]][leg_results[1]]
# dupes = set([x for x in tmp if tmp.count(x) > 1])
MATCHES = {
    'informal': 4,
}
print(MATCHES[args['video']])





# out = growcut3(img, foreground_pixels, background_pixels, iterations=10)
# out_grow3 = img.copy()
# for cord in np.ndindex(img.shape[:2]):
#     if not np.any(out[cord]):
#         out_grow3[cord] = (0,0,0)
#
# cv2.imshow('Image-GROWCUT3', out_grow3)


# label = np.zeros_like(img)
# strength = np.zeros_like(img)
# print(img.shape[0])
# print(img.shape[1])
# foreground_pixels = np.array([(535, 271), (710, 269), (760, 355), (556, 338)])
# background_pixels = np.array([(180, 196),
#     (250, 153),
#     (405, 143),
#     (524, 189),
#     (588, 186),
#     (720, 191),
#     (865, 227),
#     (889, 327),
#     (861, 432),
#     (744, 430),
#     (562, 427),
#     (457, 441),
#     (300, 437),
#     (197, 389),
#     (108, 344),
#     (57, 271),
#     (89, 254),
#     (147, 241),
#     (237, 254),
#     (340, 231),
#     (436, 227),
#     (450, 258),
#     (423, 316),
#     (312, 317)])

# for (r, c) in background_pixels:
#     label[r, c] = 0
#     strength[r, c] = 1
#
# for (r, c) in foreground_pixels:
#     label[r, c] = 1
#     strength[r, c] = 1

# state = np.zeros((2, img.shape[0], img.shape[1], 3))
#
# for (r, c) in background_pixels:
#     state[0, r, c] = 0
#     state[1, r, c] = 1
# for (r, c) in foreground_pixels:
#     state[0, r, c] = 1
#     state[1, r, c] = 1
#
# points = dict()
# for (r, c) in background_pixels:
#     points[(r, c)] = (0, 1)
# for (r, c) in foreground_pixels:
#     points[(r, c)] = (1, 1)

# out = growcut(img, np.dstack((label, strength)), window_size=5, max_iter=500)
# out = growcut3(img, foreground_pixels.tolist(), background_pixels.tolist(), iterations=10)
# kernel = np.ones((10, 10), np.uint8)
# erosion = cv2.erode(closing, kernel, iterations=3)
# cv2.imshow('Opening', opening)
# cv2.imshow('Closing', closing)
# cv2.imshow('Erosion', out)
cv2.waitKey(0)
cv2.destroyAllWindows()